/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// DatabaseSpec defines the desired state of Database
type DatabaseSpec struct {
  // Name of the required DB Quality of Service class (consult the `DatabaseClass` resources supported by the cluster)
	DatabaseClass      string            `json:"databaseClass,omitempty"`
  // [exceptional] Request to create the new DB on a specific DBOD instance
	DbodInstanceName   string            `json:"dbodInstanceName,omitempty"`
	DbName             string            `json:"dbName"`
	DbUser             string            `json:"dbUser"`
  // Labels to be assigned to the secret that will be created the operator (useful with "label selector")
	ExtraLabels        map[string]string `json:"extraLabels"`
}

// DatabaseStatus defines the observed state of Database
type DatabaseStatus struct {
	DbodInstance        string `json:"dbodInstance,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// Database is an API to request provisioning of a new database on a DBOD host, creating connection credentials in the same namespace
type Database struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DatabaseSpec   `json:"spec,omitempty"`
	Status DatabaseStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// DatabaseList contains a list of Database
type DatabaseList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Database `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Database{}, &DatabaseList{})
}
