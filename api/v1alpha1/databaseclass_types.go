/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// DatabaseClassSpec defines the desired state of DatabaseClass
type DatabaseClassSpec struct {
  Instances []string `json:"instances,omitempty"`
}

// DatabaseClassStatus defines the observed state of DatabaseClass
type DatabaseClassStatus struct {
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// DatabaseClass defines a DB QoS class as a set of DBOD hosts that can serve such DBs
type DatabaseClass struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DatabaseClassSpec   `json:"spec,omitempty"`
	Status DatabaseClassStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// DatabaseClassList contains a list of DatabaseClass
type DatabaseClassList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DatabaseClass `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DatabaseClass{}, &DatabaseClassList{})
}
