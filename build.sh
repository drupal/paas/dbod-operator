refresh-operator() {
  helm3 delete dbod-operator
  helm3 install -f __devvalues.yaml dbod-operator ~/ws/repos/helmcharts-dev/dbod-operator
}
refresh-cr() {
  kc delete -f deploy/crds/dbod.cern_v1alpha1_dbodregistration_cr.yaml
  kc create -f deploy/crds/dbod.cern_v1alpha1_dbodregistration_cr.yaml
}
bp() {(
  cd ~/ws/dbod-operator
    operator-sdk build gitlab-registry.cern.ch/drupal/paas/dbod-operator
    docker push gitlab-registry.cern.ch/drupal/paas/dbod-operator
    refresh-operator
)}

export -f refresh-operator
export -f refresh-cr
export -f bp
docker login gitlab-registry.cern.ch
