# DBOD Operator

An OpenShift cluster operator that creates Databases on preconfigured DBOD hosts.

At the moment, only __MySQL__ DBs are handled, simply because there is no existing use case for others.

#### [Presentation](https://codimd.web.cern.ch/p/BybiamapH#/)

By now the presentation is a outdated, mentioning old CRD names and local credential flows which don't exist anymore.

## Installation

Use the [helm chart](chart/dbod-operator).

## CRD (public API)

### Database

`Database` requests a new DB from DBOD and returns its admin credentials. Fields:
- `databaseClass` -> name of the required DB Quality of Service level (consult the `DatabaseClass`es supported by the cluster)
- `dbName` -> [required] name of the DB to be created
- `dbUser` -> [required] name of the user to be created for the DB
- `extraLabels` -> [required] extra labels that are passed to the DB credentials secret

#### Credentials to access the provisioned Database

The DB credentials can be found in a Secret with name `dbcredentials-${Database.Name}`.

#### DBOD instance mapping

DBOD instances are preconfigured by the cluster admins, and the selection of a specific instance to host the new DB is automatic through `databaseClass`.
In order to *override* the default selection mechanism and request a specific instance (**only for special requirements**):
- `dbodInstanceName` -> create the new DB on a specific DBOD instance *supported by the cluster*

### DatabaseClass

`DatabaseClass` specifies a DB Quality of Service level backed by a set of DBOD instances preconfigured by the cluster admins.

`Database` should request a specific `DatabaseClass` based on the service needs, decoupling from specific DBOD instances,
which should only be selected in special cases (you'll know it if you need it).

## User roles

| role | function |
| ---  | ---      |
| DB user | requests databases with specific users with `Database` |
| cluster admin | provides DBOD instance admin credentials, defines DatabaseClasses |

#### DB user

A "DB user" creates `Database`s to request DBs, specifying the `dbodClass` or exceptionally the `dbodInstanceName` to use.
To discover the supported DBOD classes: `kubectl get dbodclass`

The operator first looks for the key `dbodInstanceName` in the `Database` spec and will use the provided instance to create the database.
If this key is not found, it then looks for the `DatabaseClass` key and picks an instance from the specified `DatabaseClass` CR.
Once the DBOD instance has been selected, the operator creates the DB.

#### Cluster admin

A cluster admin needs to configure the supported DBOD instances.
This comprises 2 parts:
- Creating secrets with the DBOD admin credentials for the supported instances
- Defining DBOD QoS classes that the users can request (and potentially OPA policy restrictions)

For each DBOD instance that the cluster should be able to use, a k8s secret needs to be defined with the admin credentials:
```yaml
metadata:
  namespace: <operator_namespace>
  labels:
    dbodInstance: <instance_name>
data:
  DBODAdminPassword:
  DBODAdminUser:
  DBODInstanceHostname:
  DBODPort:
```

Notes:
- the label on the secret, which is necessary to associate it with the DBOD instance it refers to.
- the secret must be in the same NS as the operator's deployment

The DBOD instances supported in the cluster can then be pooled into DatabaseClasses according to QoS and allow random assignment of clients.
Currently, there is no controller side implementation (watches & reconcile) for the DatabaseClass CRD.

## Running the operator locally

Follow the below to run the operator locally
1. Set the Image variable: `export IMG=gitlab-registry.cern.ch/drupal/paas/dbod-operator:v1`
1. Create a secret in the operator namespace that has credentials to a DBOD instance and patch it accordingly as specified in  [DBOD Admin credentials step](###DBOD-Admin-credentials)
1. Run the make commands to build and push the docker image to the registry: `make docker-build docker-push`
1. Set the kubeconfig to point to your cluster
1. Install the CRDs and Deploy the operator: `make install deploy`
1. Create the DatabaseClass CR: `oc create -f config/samples/dbod_v1alpha1_databaseclass.yaml`
1. Create the Database CR: `oc create -f config/samples/dbod_v1alpha1_database.yaml`

- Verify the CR spec and status: `oc get Database example-database -o yaml`
- Checking logs of the operator: `oc -n openshift-cern-dbod logs -f <pod-name> -c manager`

## Verbose logs
- To enable verbose logs
   - Set the Helm value `dbodOperator.enableDebugLogs: "True"`
   - Add the following annotation with the desired log level, to the CR file definition
     ```
     annotations:
       "ansible.sdk.operatorframework.io/verbosity": "4"
     ```
